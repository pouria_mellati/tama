## Run main & tests

To run tests:

`sbt test`

To run an interactive session:

`sbt core/run`


## Random Note

Don't be alarmed if you tell the tamagotchi to go to Sleep and it refuses: the tamagotchi needs to be lying in bed and tired to agree to sleep. Similarly, other activities may have requirements. The interactive session does not explain this (or anything, really), so I thought It was worth mentioning here.