val specs2Version = "3.7.1"

lazy val commonSettings = Seq(
  scalaVersion := "2.11.7",
  scalacOptions += "-feature"
)

lazy val core = (project in file("core")).
  dependsOn(macroSub).
  settings(commonSettings: _*).
  settings(
    name := "tamagotchi",
    libraryDependencies ++= Seq(
      "org.specs2" %% "specs2-core" % specs2Version % "test",
      "org.specs2" %% "specs2-matcher-extra" % specs2Version % "test",
      "org.specs2" %% "specs2-mock" % specs2Version % "test"
    )
  )

lazy val macroSub = (project in file("macro")).
  settings(commonSettings: _*).
  settings(
    libraryDependencies += "org.scala-lang" % "scala-reflect" % scalaVersion.value
  )