package pouria.tamagotchi

import scala.language.experimental.macros
import scala.reflect.macros.whitebox
import math.{min, max}
import scala.language.implicitConversions

/**
 * Similar to Ticks. Never throws runtime errors. The addition and subtraction operators cap
 * the values to 100 and 0 respectively.
 *
 * In order to instantiate from a literal expression, use the `<literal>.percent` syntax,
 * which uses macros to ensure the literal is in the correct range.
 *
 * In order to instantiate from a non-literal expression, use `Percentage.from: Double ->
 * Option[Percentage]`.
 */
case class Percentage private (value: Double) {
  def -(other: Percentage) = Percentage(max(0, value - other.value))

  def +(other: Percentage) = Percentage(min(100, value + other.value))

  def >(other: Percentage) = value > other.value

  def <(other: Percentage) = value < other.value

  override def toString =
    s"$value%"
}

object Percentage {
  def from(d: Double): Option[Percentage] =
     if(d >= 0 && d <= 100)
       Some(Percentage(d))
     else
       None

  implicit def doubleToPercentageOps(value: Double): Percentage._PercentageOps = macro Macros.opsFromDoubleLiteral

  // For internal use only. Do not use this to create invalid Percentage instances.
  class _PercentageOps(d: Double) {
    def percent = Percentage(d)
  }

  private class Macros(val c: whitebox.Context) {
    import c.universe._

    def opsFromDoubleLiteral(value: Tree): Tree =
      value match {
        case DoubleLiteral(n) => q""" new Percentage._PercentageOps($n) """
        case _ =>
          c.abort(c.enclosingPosition, s"Expression $value does not evaluate to a Double literal in the [0 ~ 100] range.")
      }

    object DoubleLiteral {
      def unapply(i: Tree): Option[Double] =
        i match {
          case Literal(Constant(n: Double)) if n >= 0 && n <= 100 => Some(n)
          case _ => None
        }
    }
  }
}