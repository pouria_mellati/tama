package pouria.tamagotchi

import scala.language.experimental.macros
import scala.reflect.macros.whitebox
import math.max
import scala.language.implicitConversions

/**
 * Represents a natural number. Never throws runtime errors. Subtractions resulting in negative values
 * return `0.ticks`.
 *
 * In order to instantiate from literal values, use the `<literal>.ticks` syntax, which uses macros to
 * ensure only positive literal values are accepted.
 *
 * In order to instantiate from non-literal values, use `Ticks.from: Long -> Option[Ticks]`.
 */
case class Ticks private (value: Long) {
  def >(other: Ticks) =
    value > other.value

  def >=(other: Ticks) =
    value >= other.value

  def -(other: Ticks) =
    Ticks(max(0, value - other.value))

  override def toString =
    value.toString
}

object Ticks {
  def from(value: Long): Option[Ticks] =
    if(value < 0)
      None
    else
      Some(Ticks(value))

  implicit def longToTicksOps(value: Long): Ticks._TicksOps = macro Macros.opsFromNaturalLiteral

  // For internal use only. Do not use this to create invalid Ticks instances.
  class _TicksOps(l: Long) {
    def ticks: Ticks = Ticks(l)
    def tick = ticks
  }

  private class Macros(val c: whitebox.Context) {
    import c.universe._

    def opsFromNaturalLiteral(value: Tree): Tree =
      value match {
        case NatLiteral(n) => q""" new Ticks._TicksOps($n) """
        case _ =>
          c.abort(c.enclosingPosition, s"Expression $value does not evaluate to a non-negative Long literal")
      }

    object NatLiteral {
      def unapply(i: Tree): Option[Long] =
        i match {
          case Literal(Constant(n: Long)) if n >= 0 => Some(n)
          case _ => None
        }
    }
  }
}