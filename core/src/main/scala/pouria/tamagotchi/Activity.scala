package pouria.tamagotchi

import Percentage._
import Ticks._


trait Activity {
  def effectPerTick(stats: Stats): Stats
  def isApplicableTo(t: Tamagotchi): Boolean
  def followUp: Activity
}

object Activities extends Effects {
  type Effect = Stats => Stats

  private val baseEffects = Set(digest, loseHealthFromHunger, tire)

  abstract class BaseActivity(effects: Set[Effect]) extends Activity {
    override def effectPerTick(stats: Stats): Stats = {
      val combinedEffects = effects reduceOption {_ andThen _} getOrElse noEffect
      combinedEffects(stats)
    }

    override def isApplicableTo(t: Tamagotchi): Boolean = true
    override def followUp: Activity = this
  }

  case object LyingInBed extends BaseActivity(baseEffects)

  case object Sleeping extends BaseActivity(Set(digest, loseHealthFromHunger, sleep)) {
    override def isApplicableTo(t: Tamagotchi) =
      t.stats.tiredness > 70.percent &&
      (Seq(LyingInBed, Sleeping) contains t.previousActivity)
  }

  case object WakeUp extends BaseActivity(baseEffects) {
    override def isApplicableTo(t: Tamagotchi) =
      t.previousActivity == Sleeping

    override def followUp = LyingInBed
  }

  case object Chilling extends BaseActivity(baseEffects)

  case class Eating(foodLeft: Ticks) extends BaseActivity(baseEffects + eat) {
    override def isApplicableTo(t: Tamagotchi) =
      t.stats.fullness < 100.percent && foodLeft > 0.ticks

    override def followUp = Eating(foodLeft - 1.tick)
  }

  case object Pooping extends BaseActivity(baseEffects + poop) {
    override def isApplicableTo(t: Tamagotchi) =
      t.stats.poopUrge > 0.percent
  }
}

trait Effects {
  val noEffect = identity[Stats] _

  val loseHealthFromHunger = (stats: Stats) =>
    if(stats.fullness == 0.percent)
      stats.copy(health = stats.health - 10.percent)
    else
      stats

  val digest = (stats: Stats) =>
    if(stats.fullness > 0.percent)
      stats.copy(fullness = stats.fullness - 1.percent, poopUrge = stats.poopUrge + 1.percent)
    else
      stats

  val poop = (stats: Stats) =>
      stats.copy(poopUrge = stats.poopUrge - 10.percent)

  val tire = (stats: Stats) =>
    stats.copy(tiredness = stats.tiredness + 1.percent)

  val sleep = (stats: Stats) =>
    stats.copy(tiredness = stats.tiredness - 10.percent)

  val eat = (stats: Stats) =>
    stats.copy(fullness = stats.fullness + 10.percent)
}
