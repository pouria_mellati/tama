package pouria.tamagotchi

import Ticks._


sealed trait LifeStage {
  def afterOneTick: LifeStage
  def remainingTicks: Ticks
}

abstract class BaseLifeStage extends LifeStage {
  def afterOneTick: LifeStage =
    if (remainingTicks >= 1.tick)
      currentStageWithRemainingTicks(remainingTicks - 1.tick)
    else
      nextStage.afterOneTick

  protected def nextStage: LifeStage
  protected def currentStageWithRemainingTicks(ticks: Ticks): LifeStage
}

case class Infancy(remainingTicks: Ticks, nextStage: Teenhood) extends BaseLifeStage {
  override def currentStageWithRemainingTicks(ticks: Ticks): Infancy =
    copy(remainingTicks = ticks)
}

case class Teenhood(remainingTicks: Ticks, nextStage: Adulthood) extends BaseLifeStage {
  override def currentStageWithRemainingTicks(ticks: Ticks): Teenhood =
    copy(remainingTicks = ticks)
}

case class Adulthood(remainingTicks: Ticks, nextStage: Seniority) extends BaseLifeStage {
  override def currentStageWithRemainingTicks(ticks: Ticks): Adulthood =
    copy(remainingTicks = ticks)
}

case class Seniority(remainingTicks: Ticks) extends LifeStage {
  override def afterOneTick: LifeStage =
    if (remainingTicks >= 1.tick)
      Seniority(remainingTicks - 1.tick)
    else
      Death
}

case object Death extends LifeStage {
  override def afterOneTick = Death
  override def remainingTicks = 0.ticks
}