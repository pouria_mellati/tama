package pouria.tamagotchi

import Ticks._
import Activities._
import Percentage._
import scala.annotation.tailrec

object Main {
  def main(args: Array[String]) {
    println("Hello Tama!")

    val initialState = Tamagotchi(
      Infancy(20.ticks, Teenhood(40.ticks, Adulthood(40.ticks, Seniority(20.ticks)))),
      Stats(
        fullness = 50.percent,
        tiredness = 10.percent,
        poopUrge = 15.percent,
        health = 90.percent),
      previousActivity = Chilling,
      autonomousActivities = Seq(
        {stats => if(stats.tiredness > 85.percent) Some(Sleeping) else None},
        {stats => if(stats.poopUrge > 50.percent) Some(Pooping) else None}
      )
    )

    interactive(initialState)

    /**
    execute(initialState,
      Cmd(_ prompt LyingInBed, "Suggest going to bed."),
      Cmd(_ after 3.ticks, "After 3 ticks..."),
      Cmd(_ prompt Eating(2.ticks), "Nah, lets just eat!")
    )
    */
  }

  @tailrec
  def interactive(tama: Tamagotchi) {
    println(tama.prettyString)

    if(tama.isDead) {
      val cause = if(tama.lifeStage == Death) "old age" else "neglect"
      println(s"The tamagotchi has died of $cause! May it rest in peace.")
    } else {
      println("\nEnter a number and ENTER to prompt an activity, or just ENTER to pass one tick.")
      println("1: Chill, 2: Go to bed, 3: Sleep, 4: Wake up, 5: Eat, 6: Poop")
      scala.io.StdIn.readLine.trim match {
        case "1" =>
          interactive(tama prompt Chilling)
        case "2" =>
          interactive(tama prompt LyingInBed)
        case "3" =>
          interactive(tama prompt Sleeping)
        case "4" =>
          interactive(tama prompt WakeUp)
        case "5" =>
          interactive(tama prompt Eating(4.ticks))
        case "6" =>
          interactive(tama prompt Pooping)
        case "" =>
          interactive(tama after 1.tick)
        case _ =>
          println("Unrecognized input.")
          interactive(tama)
      }
    }
  }


  // Execute a sequence of state transformations, instead of going interactive.
  case class Cmd(action: Tamagotchi => Tamagotchi, description: String = "")

  def execute(state: Tamagotchi, commands: Cmd*) {
    execute(state, commands.toList)
  }

  @tailrec
  def execute(state: Tamagotchi, commands: List[Cmd]): Unit = commands match {
    case cmd::cmds =>
      if(cmd.description != "")
        println(cmd.description)

      println(state.prettyString)
      execute(cmd.action(state), cmds)
    case Nil =>
      println(state)
  }
}