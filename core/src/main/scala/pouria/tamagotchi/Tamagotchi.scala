package pouria.tamagotchi

import Activities.Chilling
import Ticks._
import Percentage._
import scala.annotation.tailrec

case class Stats(fullness: Percentage, tiredness: Percentage, poopUrge: Percentage, health: Percentage) {
  override def toString =
    s"{fullness: $fullness, tiredness: $tiredness, poopUrge: $poopUrge, health: $health}"
}

case class Tamagotchi(
    lifeStage: LifeStage,
    stats: Stats,
    previousActivity: Activity,
    autonomousActivities: Seq[Stats => Option[Activity]] = Seq(),
    promptedActivity: Option[Activity] = None) {

  @tailrec
  final def after(ticks: Ticks): Tamagotchi =
    if(ticks == 0.ticks || isDead)
      this
    else
      copy(lifeStage = lifeStage.afterOneTick,
           stats = activity.effectPerTick(stats),
           previousActivity = activity,
           promptedActivity = None
      ).after(ticks - 1.tick)

  lazy val activity =
    potentialActivitiesHighestPriorityFirst find {_ isApplicableTo this} getOrElse Chilling

  def prompt(activity: Activity): Tamagotchi =
    copy(promptedActivity = Some(activity))

  def isDead =
    lifeStage == Death ||
    stats.health == 0.percent ||
    stats.tiredness == 100.percent ||
    stats.poopUrge == 100.percent

  def prettyString = s"""
    |Tamagotchi {
    |  stats: $stats,
    |  activity: $activity,
    |  life stage: $lifeStage,
    |  prompted activity: ${promptedActivity getOrElse "None"}
    |}""".stripMargin

  private def potentialActivitiesHighestPriorityFirst: Seq[Activity] = {
    (
      promptedActivity +:
      autonomousActivities.map{_(stats)}
    ).collect {
      case Some(activity) => activity
    } :+
    previousActivity.followUp
  }
}
