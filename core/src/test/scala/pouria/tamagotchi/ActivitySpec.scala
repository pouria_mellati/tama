package pouria.tamagotchi

import org.specs2.mutable._
import org.specs2.matcher.{Matcher, MatchResult}
import Activities._
import Percentage._
import Ticks._

class ActivitySpec extends Specification {
  val stats =
    Stats(fullness = 50.percent, tiredness = 50.percent, poopUrge = 50.percent, health = 50.percent)

  val statsEmptyStomach =
    stats.copy(fullness = 0.percent)

  val tamagotchi = Tamagotchi(Adulthood(5.ticks, Seniority(3.ticks)), stats, Chilling)

  import Matchers._
  import Helpers._

  Seq(Chilling , LyingInBed) map {activity =>
    activity.toString should {
      "have expected effects" in {
        stats after activity must
          beHungrier and haveMorePoopUrge and beEquallyHealthy and beMoreTired

        statsEmptyStomach after activity must
          beEquallyFull and haveEqualPoopUrge and beLessHealthy and beMoreTired
      }

      "always be applicable" in {
        activity must beApplicableTo(tamagotchi)
      }

      "suggest self as followUp" in {
        activity.followUp must_== activity
      }
    }
  }

  "Sleeping" should {
    "have expected effects" in {
      stats after Sleeping must
        beHungrier and haveMorePoopUrge and beEquallyHealthy and beLessTired

      statsEmptyStomach after Sleeping must
        beEquallyFull and haveEqualPoopUrge and beLessHealthy and beLessTired
    }

    "only be applicable if over 70 percent tired and lying in bed or sleeping" in {
      for {
        tiredness <- Seq(71.percent, 80.percent, 100.percent)
        previousActivity <- Seq(LyingInBed, Sleeping)
        tama = tamagotchi.copy(stats = tamagotchi.stats.copy(tiredness = tiredness),
                               previousActivity = previousActivity)
      } yield {
        Sleeping must beApplicableTo(tama)
      }

      for {
        tiredness <- Seq(0.percent, 47.17.percent, 70.percent)
        previousActivity <- Seq(Chilling, LyingInBed, WakeUp, Eating(5.ticks), Pooping)
        tama = tamagotchi.copy(stats = tamagotchi.stats.copy(tiredness = tiredness),
                               previousActivity = previousActivity)
      } yield {
        Sleeping must not(beApplicableTo(tama))
      }
    }

    "suggest self as followUp" in {
      Sleeping.followUp must_== Sleeping
    }
  }

  "WakeUp" should {
    "have expected effects" in {
      stats after WakeUp must
        beHungrier and haveMorePoopUrge and beEquallyHealthy and beMoreTired

      statsEmptyStomach after WakeUp must
        beEquallyFull and haveEqualPoopUrge and beLessHealthy and beMoreTired
    }

    "only be applicable if sleeping" in {
      WakeUp must beApplicableTo(tamagotchi.copy(previousActivity = Sleeping))

      Seq(LyingInBed, WakeUp, Chilling, Eating(2.ticks), Pooping) map {invalidActivity =>
        WakeUp must not(beApplicableTo(tamagotchi.copy(previousActivity = invalidActivity)))
      }
    }

    "suggest LyingInBed as followup" in {
      WakeUp.followUp must_== LyingInBed
    }
  }

  "Eating" should {
    "have expected effects" in {
      stats after Eating(foodLeft = 3.ticks) must
        beFuller and haveMorePoopUrge and beEquallyHealthy and beMoreTired

      statsEmptyStomach after Eating(3.ticks) must
        beFuller and haveEqualPoopUrge and beLessHealthy and beMoreTired
    }

    "only be applicable if there is food left tamagotchi is not full" in {
      Eating(1.tick) must beApplicableTo(tamagotchi)
      Eating(0.ticks) must not(beApplicableTo(tamagotchi))
      Eating(1.tick) must not(beApplicableTo(tamagotchi.copy(stats = tamagotchi.stats.copy(fullness = 100.percent))))
    }

    "suggest self with 1 less food (if not already zero) as followup" in {
      Eating(3.ticks).followUp must_== Eating(2.ticks)
      Eating(0.ticks).followUp must_== Eating(0.ticks)
    }
  }

  "Pooping" should {
    "have expected effects" in {
      stats after Pooping must
        beHungrier and haveLessPoopUrge and beEquallyHealthy and beMoreTired

      statsEmptyStomach after Pooping must
        beEquallyFull and haveLessPoopUrge and beLessHealthy and beMoreTired
    }

    "only be applicable if there is poopUrge" in {
      Pooping must beApplicableTo(tamagotchi)
      Pooping must not(beApplicableTo(tamagotchi.copy(stats = tamagotchi.stats.copy(poopUrge = 0.percent))))
    }

    "suggest self as followup" in {
      Pooping.followUp must_== Pooping
    }
  }

  object Helpers {
    case class ActivityApplication(before: Stats, activity: Activity) {
      def after = activity.effectPerTick(before)
    }

    implicit class Syntax(stats: Stats) {
      def after(activity: Activity) =
        ActivityApplication(stats, activity)
    }
  }

  object Matchers {
    import Helpers.ActivityApplication

    def beMoreTired: Matcher[ActivityApplication] = (
      {appl: ActivityApplication =>
        appl.before.tiredness < appl.after.tiredness
      },
      (_: ActivityApplication).after + " is not more tired."
    )

    def beLessTired: Matcher[ActivityApplication] = (
      {appl: ActivityApplication =>
        appl.before.tiredness > appl.after.tiredness
      },
      (_: ActivityApplication).after + " is not less tired."
    )

    def beHungrier: Matcher[ActivityApplication] = (
      {appl: ActivityApplication =>
        appl.before.fullness > appl.after.fullness
      },
      (_: ActivityApplication).after + " is not less full."
    )

    def beEquallyFull: Matcher[ActivityApplication] = (
      {appl: ActivityApplication =>
        appl.before.fullness == appl.after.fullness
      },
      (_: ActivityApplication).after + " is not equally full."
    )

    def beFuller: Matcher[ActivityApplication] = (
      {appl: ActivityApplication =>
        appl.before.fullness < appl.after.fullness
      },
      (_: ActivityApplication).after + " is not fuller."
    )

    def haveMorePoopUrge: Matcher[ActivityApplication] = (
      {appl: ActivityApplication =>
        appl.before.poopUrge < appl.after.poopUrge
      },
      (_: ActivityApplication).after + " does not have more poop urge."
    )

    def haveLessPoopUrge: Matcher[ActivityApplication] = (
      {appl: ActivityApplication =>
        appl.before.poopUrge > appl.after.poopUrge
      },
      (_: ActivityApplication).after + " does not have less poop urge."
    )

    def haveEqualPoopUrge: Matcher[ActivityApplication] = (
      {appl: ActivityApplication =>
        appl.before.poopUrge == appl.after.poopUrge
      },
      (_: ActivityApplication).after + " does not have equal poop urge."
    )

    def beEquallyHealthy: Matcher[ActivityApplication] = (
      {appl: ActivityApplication =>
        appl.before.health == appl.after.health
      },
      (_: ActivityApplication).after + " is not equally healthy."
    )

    def beLessHealthy: Matcher[ActivityApplication] = (
      {appl: ActivityApplication =>
        appl.before.health > appl.after.health
      },
      (_: ActivityApplication).after + " is not less healthy."
    )

    def beApplicableTo(t: Tamagotchi): Matcher[Activity] = (
      {a: Activity => a isApplicableTo t},
      {a: Activity => s"$a is not applicable to $t."}
    )
  }
}