package pouria.tamagotchi

import org.specs2.mutable._
import org.specs2.execute._, Typecheck._
import org.specs2.matcher.TypecheckMatchers._
import Ticks._

class TicksSpec extends Specification {
  "Ticks" >> {
    "literal instantiation syntax: X.ticks" should {
      "compile with X being a natural number" in {
        typecheck {"""
          import Ticks.{longToTicksOps => literal_}

          literal_(0).ticks
          literal_(132).ticks
        """} must succeed
      }

      "not compile with X being a negative number" in {
        typecheck {"""
          import Ticks.{longToTicksOps => literal_}

          literal_(-1).ticks
        """} must not succeed

        typecheck {"""
          import Ticks.{longToTicksOps => literal_}

          literal_(-132).ticks
        """} must not succeed
      }
    }

    "from(Long)" should {
      "return Some if the parameter is positive" in {
        Seq(0, 2, 12) map {positiveVal =>
          Ticks.from(positiveVal) must beSome.which{_.value == positiveVal}
        }
      }

      "return None if the parameter is negative" in {
        Seq(-1, -12) map {negativeVal =>
          Ticks.from(negativeVal) must beNone
        }
      }
    }

    "subtraction" should {
      "subtract normally if left operand is greater than or equal to the right operand" in {
        7.ticks - 4.ticks must_== 3.ticks
        12.ticks - 12.ticks must_== 0.ticks
      }

      "return 0.ticks if the left operand is less than the right operand" in {
        3.ticks - 4.ticks must_== 0.ticks
        0.ticks - 12.ticks must_== 0.ticks
      }
    }

    "comparison operators" should {
      "compare the wrapped values" in {
        Seq(
          5.ticks -> 4.ticks,
          234.ticks -> 234.ticks,
          0.ticks -> 14.ticks,
          5.ticks -> 7.ticks
        ) map {case (left, right) =>
          left > right must_== left.value > right.value
          left >= right must_== left.value >= right.value
          left == right must_== left.value == right.value
        }
      }
    }
  }
}