package pouria.tamagotchi

import org.specs2.mutable._
import org.specs2.execute._, Typecheck._
import org.specs2.matcher.TypecheckMatchers._
import Percentage._

class PercentageSpec extends Specification {
  "Percentage" >> {
    "literal instantiation syntax: X.percent" should {
      "compile with X in the [0~100] range" in {
        typecheck {"""
          import Percentage.{doubleToPercentageOps => literal_}

          literal_(100).percent
          literal_(12.4).percent
          literal_(0).percent
        """} must succeed
      }

      "not compile with X out of the [0~100] range" in {
        typecheck {"""
          import Percentage.{doubleToPercentageOps => literal_}

          literal_(-0.12).percent
        """} must not succeed

        typecheck {"""
          import Percentage.{doubleToPercentageOps => literal_}

          literal_(101).percent
        """} must not succeed

        typecheck {"""
          import Percentage.{doubleToPercentageOps => literal_}

          literal_(2016).percent
        """} must not succeed
      }
    }

    "from(Double)" should {
      "return Some if the parameter is in the [0 ~ 100] range." in {
        Seq(0, 12.3232, 100) map {validVal =>
          Percentage.from(validVal) must beSome.which {_.value == validVal}
        }
      }

      "return None if the parameter is not in the [0 ~ 100] range" in {
        Seq(-1, -0.3232, 101, 100.12, 13324) map {
          Percentage.from(_) must beNone
        }
      }
    }

    "addition" should {
      "add normally when the sum does not exceed 100" in {
        Seq(
          4.percent -> 12.percent,
          0.percent -> 12.percent,
          12.percent -> 0.percent,
          34.123.percent -> 0.12313.percent,
          0.percent -> 0.percent,
          25.percent -> 75.percent
        ) map {case (left, right) =>
          (left + right).value must_== left.value + right.value
        }
      }

      "cap the result to 100 if it exceeds 100" in {
        Seq(
          90.percent -> 12.percent,
          100.percent -> 12.percent,
          12.percent -> 90.percent,
          34.123.percent -> 80.12313.percent
        ) map {case (left, right) =>
          left + right must_== 100.percent
        }
      }
    }

    "subtraction" should {
      "subtract normally when the left operand is greater than the right operand" in {
        Seq(
          23.45.percent -> 12.percent,
          100.percent -> 12.percent,
          90.percent -> 90.percent,
          0.percent -> 0.percent
        ) map {case (left, right) =>
          (left - right).value must_== left.value - right.value
        }
      }

      "cap the result to 0 if the left operand is less than the right operand" in {
        Seq(
          4.percent -> 12.percent,
          0.percent -> 12.percent,
          34.123.percent -> 100.percent
        ) map {case (left, right) =>
          left - right must_== 0.percent
        }
      }
    }

    "comparison operators" should {
      "compare the wrapped values" in {
        Seq(
          12.34.percent -> 12.34.percent,
          0.percent -> 56.percent,
          100.percent -> 34.6764.percent
        ) map {case (left, right) =>
          left < right must_== left.value < right.value
          left > right must_== left.value > right.value
        }
      }
    }
  }
}