package pouria.tamagotchi

import org.specs2.mutable._
import Ticks._


class LifeStageSpec extends Specification {
  "non-Death LifeStages" >> {
    "that have ticks remaining" >> {
      ".afterOneTick()" should {
        "return self with one less tick" in {
          val seniority = Seniority(4.ticks)
          val adulthood = Adulthood(4.ticks, seniority)
          val teenhood = Teenhood(4.ticks, adulthood)
          val infancy = Infancy(4.ticks, teenhood)

          Seq(infancy, teenhood, adulthood, seniority) map {stage =>
            stage.afterOneTick.remainingTicks must_== stage.remainingTicks - 1.tick
          }
        }
      }
    }

    "that have 0.ticks remaining" >> {
      ".afterOneTick()" should {
        "return the next subsequent life stage that has any ticks remaining, with one less tick" in {
          val adulthood = Adulthood(4.ticks,  Seniority(4.ticks))

          Infancy(0.ticks, Teenhood(0.ticks, adulthood)).
            afterOneTick must_== adulthood.copy(remainingTicks = 3.ticks)
        }

        "return Death if all of their subsequent life stages have 0.ticks remaining" in {
          Adulthood(0.ticks, Seniority(0.ticks)).
            afterOneTick must_== Death
        }
      }
    }
  }

  "Death.afterOneTick()" should {
    "return Death" in {
      Death.afterOneTick must_== Death
    }
  }
}