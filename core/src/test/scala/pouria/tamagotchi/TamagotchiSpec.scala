package pouria.tamagotchi

import org.specs2.mutable._
import org.specs2.mock.Mockito
import pouria.tamagotchi.Ticks._
import pouria.tamagotchi.Percentage._
import pouria.tamagotchi.Activities._

class TamagotchiSpec extends Specification with Mockito {
  type AutonomousActivity = Stats => Option[Activity]

  val tama = Tamagotchi(
    Adulthood(1.tick, Seniority(3.ticks)),
    Stats(45.percent, 54.percent, 44.percent, 87.percent),
    Chilling)

  "Tamagotchi" >> {
    "after(n.ticks)" should {
      "be the same as calling after(1.tick) n times" in {
        tama.after(0.ticks) must_== tama
        tama.after(3.ticks) must_== tama.after(1.tick).after(1.tick).after(1.tick)
      }
    }

    "after(1.tick)" should {
      "return self if it is dead" in {
        val deadTama = Tamagotchi(Death, mock[Stats], mock[Activity])
        deadTama after 1.tick must_== deadTama
      }

      "have its previous LifeStage afterOneTick(), if not dead" in {
        val lifeStage, nextLifeStage = mock[LifeStage]
        lifeStage.afterOneTick returns nextLifeStage

        tama.copy(lifeStage = lifeStage).after(1.tick).lifeStage must_== nextLifeStage
      }

      "have its stats affected by its activity, and update its previousActivity, if not dead" in {
        val activity = mock[Activity]

        val tamagotchi = tama.prompt(activity)
        activity.isApplicableTo(tamagotchi) returns true

        val affectedStats = mock[Stats]
        activity.effectPerTick(tamagotchi.stats) returns affectedStats

        tamagotchi.after(1.tick).stats must_== affectedStats
        tamagotchi.after(1.tick).previousActivity must_== activity
      }

      "reset its promptedActivity to None, if not dead" in {
        tama.prompt(Pooping).after(1.tick).promptedActivity must beNone
      }
    }


    "activity" should {
      "be the prompted activity if it exists and is applicable" in {
        val promptedActivity = mock[Activity]
        val tamagotchi = tama.prompt(promptedActivity)
        promptedActivity.isApplicableTo(tamagotchi) returns true

        tamagotchi.activity must_== promptedActivity
      }

      "be a triggered & applicable autonomous activity if no applicable prompted activity exists" in {
        val inapplicableActivity, applicableActivity = mock[Activity]
        applicableActivity.isApplicableTo(any) returns true
        inapplicableActivity.isApplicableTo(any) returns false

        val untriggeredAuto, inapplicableAuto, applicableAuto = mock[AutonomousActivity]
        untriggeredAuto(any) returns None
        inapplicableAuto(any) returns Some(inapplicableActivity)
        applicableAuto(any) returns Some(applicableActivity)

        val tamagotchiNoPrompt =
          tama.copy(
            autonomousActivities = Seq(untriggeredAuto, inapplicableAuto, applicableAuto),
            promptedActivity = None)

        val tamagotchiInapplicablePrompt = tamagotchiNoPrompt.prompt(inapplicableActivity)

        tamagotchiNoPrompt.activity must_== applicableActivity
        tamagotchiInapplicablePrompt.activity must_== applicableActivity
      }

      "give priority to autonomous activities that occur earlier in 'autonomousActivities'" in {
        val auto1, auto2 = mock[AutonomousActivity]
        val activity1, activity2 = mock[Activity]
        auto1(any) returns Some(activity1)
        auto2(any) returns Some(activity2)

        Seq(activity1, activity2) foreach {_.isApplicableTo(any) returns true}

        val tamaWithAutoActivitiesOrder1 = tama.copy(autonomousActivities = Seq(auto1, auto2))
        val tamaWithAutoActivitiesOrder2 = tama.copy(autonomousActivities = Seq(auto2, auto1))

        tamaWithAutoActivitiesOrder1.activity must_== activity1
        tamaWithAutoActivitiesOrder2.activity must_== activity2
      }

      "be the follow up of the previousActivity if there are no applicable autonomous or prompted activities" in {
        val previousActivity, followUpActivity, inapplicablePrompt = mock[Activity]
        previousActivity.followUp returns followUpActivity
        followUpActivity.isApplicableTo(any) returns true
        inapplicablePrompt.isApplicableTo(any) returns false

        val untriggeredAuto = mock[AutonomousActivity]
        untriggeredAuto(any) returns None

        for {
          prompt <- Seq(None, Some(inapplicablePrompt))
          autoActivities <- Seq(Seq.empty[AutonomousActivity], Seq(untriggeredAuto))
        } yield {
          val tamagotchi = tama.copy(promptedActivity = prompt,
                                     autonomousActivities = autoActivities,
                                     previousActivity = previousActivity)

          tamagotchi.activity must_== followUpActivity
        }
      }

      "be Chilling if there is no applicable prompted, autonomous or followup activity" in {
        val inapplicablePrompt, previousActivity, followUpActivity = mock[Activity]
        previousActivity.followUp returns followUpActivity

        Seq(inapplicablePrompt, followUpActivity) foreach {
          _.isApplicableTo(any) returns false
        }

        val untriggeredAuto = mock[AutonomousActivity]
        untriggeredAuto(any) returns None

        for {
          prompt <- Seq(None, Some(inapplicablePrompt))
          autoActivities <- Seq(Seq.empty[AutonomousActivity], Seq(untriggeredAuto))
        } yield {
          val tamagotchi = tama.copy(promptedActivity = prompt,
                                     autonomousActivities = autoActivities,
                                     previousActivity = previousActivity)

          tamagotchi.activity must_== Chilling
        }
      }
    }


    "prompt(Activity)" should {
      "simply set the promptedActivity field" in {
        val promptedActivity = mock[Activity]
        tama.prompt(promptedActivity).promptedActivity must_== Some(promptedActivity)
      }
    }


    "isDead" should {
      "be true if the stats are fatal: 100% tiredness or 100% poopUrge or 0% health" in {
        Seq(
          Stats(50.percent, 50.percent, 50.percent, health = 0.percent),
          Stats(50.percent, tiredness = 100.percent, 50.percent, 50.percent),
          Stats(50.percent, 50.percent, poopUrge = 100.percent, 50.percent)
        ) map {fatalStats =>
          tama.copy(stats = fatalStats).isDead must_== true
        }
      }

      "be true if the lifeStage is Death" in {
        tama.copy(lifeStage = Death).isDead must_== true
      }

      "be false if the stats are not fatal and the lifeStage is not Death" in {
        val nonDeadlyFullnesses = Seq(0.percent, 50.percent, 100.percent)
        val nonDeadlyTirednesses = Seq(0.percent, 50.percent, 99.percent)
        val nonDeadlyPoopUrges = Seq(0.percent, 50.percent, 99.percent)
        val nonDeadlyHealthLevels = Seq(1.percent, 50.percent, 100.percent)
        val nonDeadlyLifeStages = {
          val seniority = Seniority(1.tick)
          val adulthood = Adulthood(1.tick, seniority)
          val teenhood = Teenhood(1.tick, adulthood)
          val infancy = Infancy(1.tick, teenhood)

          Seq(infancy, teenhood, adulthood, seniority)
        }

        for {
          fullness <- nonDeadlyFullnesses
          tiredness <- nonDeadlyTirednesses
          poopUrge <- nonDeadlyPoopUrges
          health <- nonDeadlyHealthLevels
          lifeStage <- nonDeadlyLifeStages
        } yield {
          val stats = Stats(fullness = fullness, tiredness = tiredness, poopUrge = poopUrge, health = health)

          tama.copy(stats = stats, lifeStage = lifeStage).isDead must_== false
        }
      }
    }
  }
}